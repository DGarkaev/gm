package com.m55.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by dgarkaev on 24.06.15.
 */
@Converter(autoApply=true)
public class BoolConverter implements AttributeConverter<Boolean,Integer> {

    @Override
    public Integer convertToDatabaseColumn(Boolean aBoolean) {
        if (aBoolean == null)
        return null;
        return aBoolean?1:0;
    }

    @Override
    public Boolean convertToEntityAttribute(Integer integer) {
        if (integer == null)
        return null;
        return (integer==1);
    }
}
