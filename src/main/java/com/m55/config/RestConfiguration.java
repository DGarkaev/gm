package com.m55.config;

import com.m55.DAO.entity.Bonus;
import com.m55.DAO.entity.Card;
import com.m55.DAO.entity.Tarif;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import java.net.URI;

/**
 * Created by dgarkaev on 27.07.15.
 */
@Configuration
public class RestConfiguration  extends RepositoryRestMvcConfiguration {

    @Override
    public RepositoryRestConfiguration config() {
        RepositoryRestConfiguration config = super.config();
        config.setBaseUri(URI.create("/hal"));
        config.exposeIdsFor(Card.class, Tarif.class, Bonus.class);
        return config;
    }
}
