package com.m55.DAO.entity;


import com.m55.converter.BoolConverter;

import javax.persistence.*;
import java.sql.Time;

/**
 * Created by dgarkaev on 02.06.15.
 */

/*
bonus.dob,500,"бонус ДР"
bonus.dob.day,14,"длительность бонуса ДР"
bonus.dob.enable,0,"активность бонуса ДР"

+bonus.timeout,365,"время действия бонуса"
+bonus.lock.day,7,"время блокировки бонусов"
+bonus.part.%,30,"какой процент от суммы можно оплатить бонусами"

+bonus.time.%,2,"бонус времени %"
+bonus.time.enable,1,"активность бонуса времени"
+bonus.time.finish,16:00,"конец бонуса времени"
+bonus.time.start,14:00,"начала бонуса времени"


+status.Ag,50000,"серебрянный статус карты"
+status.Ag.%,3,"% по серебрянной карте"
+status.Au,150000,"золотой статус карты"
+status.Au.%,5,"% по золотой карте"
-status.Pt,inf,"платиновый статус карты"
+status.Pt.%,7,"% по платиновой карте"
*/
@Entity
@Table(name="tarif")
public class Tarif {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "bonus_timeout")
    private Integer bonusTimeout;

    @Column(name = "bonus_lock_days")
    private Integer bonusLockDays;

    @Column(name = "bonus_part_pay")
    private Integer bonusPartPay;


    @Column(name = "status_ag")
    private Double statusAg;
    @Column(name = "status_ag_persent")
    private Double statusAgPersent;

    @Column(name = "status_au")
    private Double statusAu;
    @Column(name = "status_au_persent")

    private Double statusAuPersent;
    @Column(name = "status_pt_persent")
    private Double statusPtPersent;

    @Column(name = "bonus_time_enable", columnDefinition = "TINYINT", length = 1)
    @Convert(converter = BoolConverter.class)
    private Boolean bonusTimeEnable;
    @Column(name = "bonus_time_persent")
    private Double bonusTimePersent;
    @Column(name = "bonus_time_begin")
    private Time bonusTimeBegin;
    @Column(name = "bonus_time_end")
    private Time bonusTimeEnd;

    @Column(name = "bonus_dob_enable", columnDefinition = "TINYINT", length = 1)
    @Convert(converter = BoolConverter.class)
    private Boolean bonusDobEnable;
    @Column(name = "bonus_dob")
    private Double bonusDob;
    @Column(name = "bonus_dob_days")
    private Integer bonusDobDays;

  public Tarif(){
  }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBonusTimeout() {
        return bonusTimeout;
    }

    public void setBonusTimeout(Integer bonusTimeout) {
        this.bonusTimeout = bonusTimeout;
    }

    public Integer getBonusLockDays() {
        return bonusLockDays;
    }

    public void setBonusLockDays(Integer bonusLockDays) {
        this.bonusLockDays = bonusLockDays;
    }

    public Integer getBonusPartPay() {
        return bonusPartPay;
    }

    public void setBonusPartPay(Integer bonusPartPay) {
        this.bonusPartPay = bonusPartPay;
    }

    public Double getStatusAg() {
        return statusAg;
    }

    public void setStatusAg(Double statusAg) {
        this.statusAg = statusAg;
    }

    public Double getStatusAgPersent() {
        return statusAgPersent;
    }

    public void setStatusAgPersent(Double statusAgPersent) {
        this.statusAgPersent = statusAgPersent;
    }

    public Double getStatusAu() {
        return statusAu;
    }

    public void setStatusAu(Double statusAu) {
        this.statusAu = statusAu;
    }

    public Double getStatusAuPersent() {
        return statusAuPersent;
    }

    public void setStatusAuPersent(Double statusAuPersent) {
        this.statusAuPersent = statusAuPersent;
    }

    public Double getStatusPtPersent() {
        return statusPtPersent;
    }

    public void setStatusPtPersent(Double statusPtPersent) {
        this.statusPtPersent = statusPtPersent;
    }

    public Boolean getBonusTimeEnable() {
        return bonusTimeEnable;
    }

    public void setBonusTimeEnable(Boolean bonusTimeEnable) {
        this.bonusTimeEnable = bonusTimeEnable;
    }

    public Double getBonusTimePersent() {
        return bonusTimePersent;
    }

    public void setBonusTimePersent(Double bonusTimePersent) {
        this.bonusTimePersent = bonusTimePersent;
    }

    public Time getBonusTimeBegin() {
        return bonusTimeBegin;
    }

    public void setBonusTimeBegin(Time bonusTimeBegin) {
        this.bonusTimeBegin = bonusTimeBegin;
    }

    public Time getBonusTimeEnd() {
        return bonusTimeEnd;
    }

    public void setBonusTimeEnd(Time bonusTimeEnd) {
        this.bonusTimeEnd = bonusTimeEnd;
    }

    public Boolean getBonusDobEnable() {
        return bonusDobEnable;
    }

    public void setBonusDobEnable(Boolean bonusDobEnable) {
        this.bonusDobEnable = bonusDobEnable;
    }

    public Double getBonusDob() {
        return bonusDob;
    }

    public void setBonusDob(Double bonusDob) {
        this.bonusDob = bonusDob;
    }

    public Integer getBonusDobDays() {
        return bonusDobDays;
    }

    public void setBonusDobDays(Integer bonusDobDays) {
        this.bonusDobDays = bonusDobDays;
    }

    @Override
    public String toString()
    {
        return String.format("id:%d\tname:%s",id,name);
    }


}
