package com.m55.DAO.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.m55.converter.JsonDateSerializer;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by dgarkaev on 03.06.15.
 */


@Entity
@Table(name = "card")
public class Card /*extends ResourceSupport*/{
    @Id
    @Column(name = "card_number")
    //@JsonProperty("id")
    private String cardNumber;

    @Column(name = "dob")
    @JsonSerialize(using = JsonDateSerializer.class, as = Date.class)
    private Date dayOfBirth;

    @Column(name = "surname", length = 50)
    private String surname;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "patronymic", length = 50)
    private String patronymic;

    @Column(name = "phone", length = 12)
    private String phone;

    @Column(name = "dt_create")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp cardRegistry;

    @OneToMany(mappedBy = "card", cascade = CascadeType.ALL)
    @OrderBy("date_of_cost DESC")
    private List<Bonus> bonus;

    public List<Bonus> getBonus() {
        return bonus;
    }

    public void setBonus(List<Bonus> bonus) {
        this.bonus = bonus;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tarif_id" )
    private Tarif tarif;

    public Card() {

    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(Date dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Timestamp getCardRegistry() {
        return cardRegistry;
    }

    public void setCardRegistry(Timestamp cardRegistry) {
        this.cardRegistry = cardRegistry;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }
}

