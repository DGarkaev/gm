package com.m55.DAO.entity;

import com.m55.DAO.repository.BonusRepository;
import com.m55.controller.CardController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by dgarkaev on 30.07.15.
 */
public class Order {

    @Autowired
    CardController cardController;

    @Autowired
    BonusRepository bonusRepository;

    private String cardId;
    private Double cost;

    @DateTimeFormat(pattern="HH:mm")
    private Date time;

    public Order() {
    }

    public Order(String cardId, Double cost, Date time) {
        this.cardId = cardId;
        this.cost = cost;
        this.time = time;
    }


    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    //------------------------------------------------------------------------------------------------------------------
    private void calcOrder()
    {
        //cardController.validate(cardId);

    }
}
