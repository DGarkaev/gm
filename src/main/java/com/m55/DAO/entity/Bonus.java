package com.m55.DAO.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.m55.converter.JsonTimeWithNullSerializer;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by dgarkaev on 03.06.15.
 */

@Entity
@Table(name = "bonus")
public class Bonus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_id" )
    private Card card;

    @Column(name = "date_of_cost")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    private Timestamp dateOfCost;

    @Column(name = "time_of_visit")
    @JsonSerialize(using = JsonTimeWithNullSerializer.class, as = Time.class)
    private Time timeOfVisit;

    @Column(name = "cost")
    private Double cost;

    @Column(name = "bonus")
    private Double bonus;

    @Column(name = "bonus_apply")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm")
    private Timestamp bonusApply;

    public Bonus() {}

    public Bonus(Double cost, Time timeOfVisit, Double bonus) {
        this.timeOfVisit = timeOfVisit;
        this.cost = cost;
        this.bonus = bonus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Timestamp getDateOfCost() {
        return dateOfCost;
    }

    public void setDateOfCost(Timestamp dateOfCost) {
        this.dateOfCost = dateOfCost;
    }

    public Time getTimeOfVisit() {
        return timeOfVisit;
    }

    public void setTimeOfVisit(Time timeOfVisit) {
        this.timeOfVisit = timeOfVisit;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    @Access(AccessType.PROPERTY)
    @Column(name = "bonus_apply", nullable = true)
    public Timestamp getBonusApply() {
        return bonusApply;
    }
    public void setBonusApply(Timestamp bonusApply) {
        this.bonusApply = bonusApply;
    }

/*    @Override
    public String toString()
    {
        return String.format("id:%d cost:%.2fр. binus:%.2f",id,cost,bonus);
    }*/
}
