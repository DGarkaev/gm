package com.m55.DAO.repository;

import com.m55.DAO.entity.Tarif;
import org.springframework.data.repository.PagingAndSortingRepository;

//@RepositoryRestResource()
public interface TarifRepository extends PagingAndSortingRepository<Tarif,Long> {

}
