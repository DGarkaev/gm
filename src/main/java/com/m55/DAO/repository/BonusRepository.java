package com.m55.DAO.repository;


import com.m55.DAO.entity.Bonus;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by dgarkaev on 19.06.15.
 */
//@RepositoryRestResource
public interface BonusRepository extends PagingAndSortingRepository<Bonus,Long> {

    /*
    SELECT
    COALESCE(SUM(b.bonus), 0) INTO ba
  FROM bonus b
  WHERE b.card_id = card_id
  AND (DATEDIFF(dn, b.date_of_cost) >= (SELECT
      c.value
    FROM config c
    WHERE c.name = 'bonus.lock.day' LIMIT 1))
  AND (DATEDIFF(dn, b.date_of_cost) <= (SELECT
      c.value
    FROM config c
    WHERE c.name = 'bonus.timeout' LIMIT 1)) LIMIT 1;

  RETURN ba;*/
/*
    @Query("SELECT COALESCE(SUM (b.bonus), 0) FROM bonus b WHERE b.card_id = :cardId")
    public double findByAllBonus(@Param("cardId") String cardId);
*/
}

