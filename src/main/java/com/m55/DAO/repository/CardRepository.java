package com.m55.DAO.repository;

import com.m55.DAO.entity.Card;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by dgarkaev on 22.06.15.
 */

//@RepositoryRestResource
public interface CardRepository extends PagingAndSortingRepository<Card,String> {
    @Query("FROM Card WHERE CONCAT(card_number,surname,name,patronymic) LIKE %:cardPart%")
    List<Card> findByPart(@Param("cardPart") String cardPart);
}

