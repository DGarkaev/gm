package com.m55.frontend.controller;

import com.m55.controller.CardController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dgarkaev on 05.08.15.
 */

@Controller
@RequestMapping(value = "/")
public class GmController {
    @Autowired
    CardController cardController;

    @RequestMapping("/")
    String index(Model model) {
/*        model.addAttribute("head1", "Golden Mean");
        model.addAttribute("head2", "система управления бонусами");*/
        return "index";
    }

    @RequestMapping(value = "/index1", method = RequestMethod.GET)
    String index2(/*@RequestParam(value = "name", required = false, defaultValue = "World") String name, */Model model) {
/*
        model.addAttribute("name", name);
        List<Card> cardList = cardController.getCards();
        model.addAttribute("allCard", cardList);
*/

        return "index1";
    }

    @RequestMapping(value = "/card/{cardId}", method = RequestMethod.GET)
    String card(@PathVariable(value = "cardId") String cardId, Model model) {
        //Card card = cardController.getCard(cardId);
        //model.addAttribute("card", new CardFullInfo(cardId));
        model.addAttribute(cardId);
        return "card";
    }

    @RequestMapping(value = "/tarifs", method = RequestMethod.GET)
    String getTarifs(/*@RequestParam(value = "name", required = false, defaultValue = "World") String name, */Model model) {
        return "tarifs";
    }
}
