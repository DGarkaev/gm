package com.m55.REST.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by dgarkaev on 31.07.15.
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CardNotFoundException extends RuntimeException{
    public CardNotFoundException(String cardId) {
        super("Could not find Card '" + cardId + "'.");
    }
}
