package com.m55.REST.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.ParseException;

/**
 * Created by dgarkaev on 13.08.15.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TimeConvertException extends RuntimeException {
    public TimeConvertException(ParseException e) {
        super(new StringBuilder().append(e.getMessage()).append(", pos: [").append(e.getErrorOffset() + 1).append("]").toString());
        //super(e.getMessage());
    }
}
