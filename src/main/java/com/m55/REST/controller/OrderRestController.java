package com.m55.REST.controller;

import com.m55.DAO.entity.Order;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Created by dgarkaev on 30.07.15.
 */
@RestController
@RequestMapping(value = "/order")
public class OrderRestController {


    @RequestMapping(value="/{card_id}", method = RequestMethod.GET)
    public ResponseEntity<Object> makeOrder(@PathVariable(value = "card_id") String cardId,
                           /*HttpServletRequest request*/
                           @RequestParam(value = "cost", required=true)  Double cost,
                           @RequestParam(value = "time", required=true) @DateTimeFormat(pattern="HHmm") Date time
    ) {
        //Map<String, String[]> parameters = request.getParameterMap();
        //double _cost = Double.valueOf(cost);
        //
        //Time _time = Time.valueOf(time);
        Order order = new Order(cardId, cost, time);
//        if (!true)
//        {
//            return new ResponseEntity<>( "Custom user message",  HttpStatus.INTERNAL_SERVER_ERROR);
//    }
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

/*
    @RequestMapping(method = RequestMethod.GET, value = "/{card_id}/{cost}/{time}")
    public Order makeOrder(Model model,@PathVariable(value = "card_id") final String cardId,
                           @PathVariable(value = "cost") final Double cost,
                           @PathVariable(value = "time")  @DateTimeFormat(pattern="HHmm") final Time time)
    {
        return new Order(cardId, cost, null);
    }
*/
}
/*
 @ResponseBody ResponseEntity<? extends AbstractResponse> createUser(@RequestBody String requestBody) {
    if(!valid(requestBody) {
        ErrorResponse errResponse = new ErrorResponse();
        //populate with error information
        return new ResponseEntity<ErrorResponse> (errResponse, HTTPStatus.BAD_REQUEST);
    }
    createUser();
    CreateUserSuccessResponse successResponse = new CreateUserSuccessResponse();
    // populate with more info
    return new ResponseEntity<CreateUserSuccessResponse> (successResponse, HTTPSatus.OK);
}
*/