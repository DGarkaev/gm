package com.m55.REST.controller;

import com.m55.DAO.entity.Tarif;
import com.m55.DAO.repository.TarifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.util.Date;
import java.util.List;


@RestController
public class TarifRestController {

    @Autowired
    private TarifRepository tarifRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/gettarifs")
    public List<Tarif> getTarifs() {
        return (List<Tarif>) tarifRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/gettarif")
    public Tarif getTarifs(@RequestParam(value = "id", required = true) long id, Model model) {
        model.addAttribute("id", id);
        return tarifRepository.findOne(id);
    }

    @RequestMapping(value = "/tarif/addtarif", method = RequestMethod.POST)
    public Tarif createTarif(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "bonusLockDays") Integer bonusLockDays,
            @RequestParam(value = "bonusTimeout") Integer bonusTimeout,
            @RequestParam(value = "bonusPartPay") Integer bonusPartPay,
            @RequestParam(value = "statusAg") Double statusAg,
            @RequestParam(value = "statusAgPersent") Double statusAgPersent,
            @RequestParam(value = "statusAu") Double statusAu,
            @RequestParam(value = "statusAuPersent") Double statusAuPersent,
            @RequestParam(value = "statusPtPersent") Double statusPtPersent,
            @RequestParam(value = "bonusTimeEnable") Boolean bonusTimeEnable,
            @RequestParam(value = "bonusTimePersent") Double bonusTimePersent,
            @RequestParam(value = "bonusTimeBegin") @DateTimeFormat(pattern = "HH:mm") Date bonusTimeBegin,
            @RequestParam(value = "bonusTimeEnd") @DateTimeFormat(pattern = "HH:mm") Date bonusTimeEnd,
            @RequestParam(value = "bonusDobEnable") Boolean bonusDobEnable,
            @RequestParam(value = "bonusDob") Double bonusDob,
            @RequestParam(value = "bonusDobDays") Integer bonusDobDays) {
        Tarif tarif = new Tarif();
        tarif.setName(name);
        tarif.setBonusLockDays(bonusLockDays);
        tarif.setBonusTimeout(bonusTimeout);
        tarif.setBonusPartPay(bonusPartPay);
        tarif.setStatusAg(statusAg);
        tarif.setStatusAgPersent(statusAgPersent);
        tarif.setStatusAu(statusAu);
        tarif.setStatusAuPersent(statusAuPersent);
        tarif.setStatusPtPersent(statusPtPersent);
        tarif.setBonusTimeEnable(bonusTimeEnable);
        tarif.setBonusTimePersent(bonusTimePersent);
        tarif.setBonusTimeBegin(new Time(bonusTimeBegin.getTime()));
        tarif.setBonusTimeEnd(new Time(bonusTimeEnd.getTime()));
        tarif.setBonusDobEnable(bonusDobEnable);
        tarif.setBonusDob(bonusDob);
        tarif.setBonusDobDays(bonusDobDays);

        return tarifRepository.save(tarif);
    }

    @RequestMapping(value = "/tarif/edittarif", method = RequestMethod.POST)
    public Tarif editTarif(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "bonusLockDays") Integer bonusLockDays,
            @RequestParam(value = "bonusTimeout") Integer bonusTimeout,
            @RequestParam(value = "bonusPartPay") Integer bonusPartPay,
            @RequestParam(value = "statusAg") Double statusAg,
            @RequestParam(value = "statusAgPersent") Double statusAgPersent,
            @RequestParam(value = "statusAu") Double statusAu,
            @RequestParam(value = "statusAuPersent") Double statusAuPersent,
            @RequestParam(value = "statusPtPersent") Double statusPtPersent,
            @RequestParam(value = "bonusTimeEnable") Boolean bonusTimeEnable,
            @RequestParam(value = "bonusTimePersent") Double bonusTimePersent,
            @RequestParam(value = "bonusTimeBegin") @DateTimeFormat(pattern = "HH:mm") Date bonusTimeBegin,
            @RequestParam(value = "bonusTimeEnd") @DateTimeFormat(pattern = "HH:mm") Date bonusTimeEnd,
            @RequestParam(value = "bonusDobEnable") Boolean bonusDobEnable,
            @RequestParam(value = "bonusDob") Double bonusDob,
            @RequestParam(value = "bonusDobDays") Integer bonusDobDays) {
        Tarif tarif = tarifRepository.findOne(id);
        tarif.setName(name);
        tarif.setBonusLockDays(bonusLockDays);
        tarif.setBonusTimeout(bonusTimeout);
        tarif.setBonusPartPay(bonusPartPay);
        tarif.setStatusAg(statusAg);
        tarif.setStatusAgPersent(statusAgPersent);
        tarif.setStatusAu(statusAu);
        tarif.setStatusAuPersent(statusAuPersent);
        tarif.setStatusPtPersent(statusPtPersent);
        tarif.setBonusTimeEnable(bonusTimeEnable);
        tarif.setBonusTimePersent(bonusTimePersent);
        tarif.setBonusTimeBegin(new Time(bonusTimeBegin.getTime()));
        tarif.setBonusTimeEnd(new Time(bonusTimeEnd.getTime()));
        tarif.setBonusDobEnable(bonusDobEnable);
        tarif.setBonusDob(bonusDob);
        tarif.setBonusDobDays(bonusDobDays);

        return tarifRepository.save(tarif);
    }
    @RequestMapping(value = "/tarif/{id}", method = RequestMethod.DELETE)
    public void deleteTarif(@PathVariable(value = "id") long tarifId) {
        //Tarif tarif = tarifRepository.findOne(tarifId);
        tarifRepository.delete(tarifId);
    }
}
