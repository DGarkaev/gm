package com.m55.REST.controller;

import com.m55.DAO.entity.Bonus;
import com.m55.DAO.entity.Card;
import com.m55.DAO.entity.Tarif;
import com.m55.DAO.repository.TarifRepository;
import com.m55.controller.CardController;
import com.m55.helper.CardInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by dgarkaev on 29.07.15.
 */

@RestController
//@RepositoryRestController
@RequestMapping(value = "/card", produces = "application/json")
public class CardRestController {

    @Autowired
    CardController cardController;

    @Autowired
    TarifRepository tarifRepository;

    @RequestMapping(method = RequestMethod.POST)
    public Card addCard(@RequestParam(value = "cardid", required = true) String cardid,
                        @RequestParam(value = "tarif", required = true) long tarifId,
                        @RequestParam(value = "dob", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dob,
                        @RequestParam(value = "name", required = true) String name,
                        @RequestParam(value = "lastname", required = true) String lastName,
                        @RequestParam(value = "surname", required = false, defaultValue = "") String surName) {
        Card card = new Card();
        card.setCardNumber(cardid);
        Tarif tarif = tarifRepository.findOne(tarifId);
        card.setTarif(tarif);
        card.setDayOfBirth(new java.sql.Date(dob.getTime()));
        card.setName(name);
        card.setSurname(lastName);
        card.setPatronymic(surName);
        cardController.save(card);
        return card;
    }

    //TODO: Вместо POST должно быть PUT, но не работает
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Card updateCard(@PathVariable(value = "id") String cardId,
                           @RequestParam(value = "dob", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dob,
                           @RequestParam(value = "name", required = true) String name,
                           @RequestParam(value = "lastname", required = true) String lastName,
                           @RequestParam(value = "surname", required = false, defaultValue = "") String surName,
                           @RequestParam(value = "tarif", required = true) long tarifId) {
        Card card = cardController.getCard(cardId);
        //?????card.setCardNumber(cardid);
        Tarif tarif = tarifRepository.findOne(tarifId);
        card.setTarif(tarif);
        card.setDayOfBirth(new java.sql.Date(dob.getTime()));
        card.setName(name);
        card.setSurname(lastName);
        card.setPatronymic(surName);
        cardController.save(card);
        return card;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Resource<Card>> getAllCards() {
        List<Resource<Card>> resourceCardList = new ArrayList<>();
        List<Card> cardList = cardController.getCards();
        for (Card card : cardList) {
            Resource<Card> cardResource = new Resource<>(card);
            cardResource.add(linkTo(methodOn(CardRestController.class).getCard(card.getCardNumber())).withSelfRel());
            cardResource.add(linkTo(methodOn(CardRestController.class).getTarif(card.getCardNumber())).withRel("tarif"));
            cardResource.add(linkTo(methodOn(CardRestController.class).getCardInfo(card.getCardNumber())).withRel("info"));

            resourceCardList.add(cardResource);
        }

        return resourceCardList;
    }


/*

    @RequestMapping(value = "/v2", method = RequestMethod.GET)
    public String getAllCards1() {
        Gson gson = new Gson();
        Card card = cardController.getCard("00001");
        String json = gson.toJson(card);
        return json;

    }
*/


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Resource<Card> getCard(@PathVariable(value = "id") String cardId) {
        Resource<Card> cardResource = new Resource<>(cardController.getCard(cardId));

        cardResource.add(linkTo(methodOn(CardRestController.class).getCard(cardId)).withSelfRel());
        cardResource.add(linkTo(methodOn(CardRestController.class).getTarif(cardId)).withRel("tarif"));
        cardResource.add(linkTo(methodOn(CardRestController.class).getCardInfo(cardId)).withRel("info"));
/*
        cardResource.add(
                new Link(
                        linkTo(CardRestController.class).slash(cardResource.getContent().getCardNumber()).slash(cardResource.getContent().getTarif()).slash("tarif")
                                .toUriComponentsBuilder().build().toUriString() + "/",
                        "Get card tarif"
                )
        );
*/

        return cardResource;
    }

    @RequestMapping(value = "/{id}/tarif")
    public Resource<Tarif> getTarif(@PathVariable(value = "id") String cardId) {
        Card card = cardController.getCard(cardId);
        Resource<Tarif> tarifResource = new Resource<Tarif>(card.getTarif());
        tarifResource.add(linkTo(methodOn(CardRestController.class).getTarif(cardId)).withSelfRel());
        return tarifResource;
    }

    @RequestMapping(value = "/{id}/info")
    public Resource<CardInfo> getCardInfo(@PathVariable(value = "id") String cardId) {
        Resource<CardInfo> cardInfoResource = new Resource<CardInfo>(cardController.getCardInfo(cardId));
        cardInfoResource.add(linkTo(methodOn(CardRestController.class).getCardInfo(cardId)).withSelfRel());
        return cardInfoResource;
    }
    /*    @RequestMapping(value = "/findcard", method = RequestMethod.POST)
        public List<Card> findCard(@RequestParam(value = "cardpart") String cardPart, Model model)
        {
            return cardController.findCard(cardPart);
        }*/

    @RequestMapping(value = "/search")
    public List<Resource<Card>> findCards(@RequestParam(value = "cardpart") String cardPart) {
        List<Resource<Card>> resourceCardList = new ArrayList<>();
        List<Card> cardList = cardController.searchCard(cardPart);
        for (Card card : cardList) {
            Resource<Card> cardResource = new Resource<>(card);
            cardResource.add(linkTo(methodOn(CardRestController.class).getCard(card.getCardNumber())).withSelfRel());
            cardResource.add(linkTo(methodOn(CardRestController.class).getTarif(card.getCardNumber())).withRel("tarif"));
            cardResource.add(linkTo(methodOn(CardRestController.class).getCardInfo(card.getCardNumber())).withRel("info"));

            resourceCardList.add(cardResource);
        }

        return resourceCardList;
    }


    @RequestMapping(value = "/{id}/calcorder")
    public Map<String, Double> getCalcOrder(@PathVariable(value = "id") String cardId, @RequestParam(value = "cost", required = true) double cost, @RequestParam(value = "time", required = false, defaultValue = "00:00") String time) {
        return cardController.calcOrder(cardId, cost, time);
    }

    @RequestMapping(value = "/{id}/addbonus", method = RequestMethod.POST)
    public Bonus addBonus(@PathVariable(value = "id") String cardId, @RequestParam(value = "cost", required = true) double cost, @RequestParam(value = "time", required = false, defaultValue = "00:00") String time) {
        return cardController.addBonus(cardId, cost, time);
    }

    @RequestMapping(value = "/{id}/applybonus", method = RequestMethod.POST)
    public boolean applyBonus(@PathVariable(value = "id") String cardId, @RequestParam(value = "cost", required = true) double cost, @RequestParam(value = "time", required = false, defaultValue = "00:00") String time) {
        return cardController.applyBonus(cardId, cost, time);
    }

    @RequestMapping(value = "/{id}/transactions", method = RequestMethod.GET)
    public List<Bonus> getTransactions(@PathVariable(value = "id") String cardId) {
        return cardController.getAllBonuses(cardId);
    }
}
