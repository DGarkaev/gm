package com.m55.helper;

/**
 * Created by dgarkaev on 31.07.15.
 */
public class CardInfo {
    private double bonusAll;
    private double bonusAccess;
    private double persent;
    private double costSum;

    public double getCostSum() {
        return costSum;
    }

    public CardInfo setCostSum(double costSum) {
        this.costSum = costSum;
        return this;
    }

    public CardInfo setBonusAll(double bonusAll) {
        this.bonusAll = bonusAll;
        return this;
    }

    public CardInfo setBonusAccess(double bonusAccess) {
        this.bonusAccess = bonusAccess;
        return this;
    }

    public CardInfo setPersent(double persent) {
        this.persent = persent;
        return this;
    }

    public double getBonusAccess() {
        return bonusAccess;
    }

    public double getPersent() {
        return persent;
    }

    public double getBonusAll() {
        return bonusAll;
    }

}
