package com.m55.controller;

import com.m55.DAO.entity.Bonus;
import com.m55.DAO.entity.Card;
import com.m55.DAO.entity.Tarif;
import com.m55.DAO.repository.CardRepository;
import com.m55.REST.error.CardNotFoundException;
import com.m55.REST.error.TimeConvertException;
import com.m55.helper.CardInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dgarkaev on 31.07.15.
 */
//ToDo: Replace Card --> CardRest
@Component
public class CardController {
    @Autowired
    CardRepository cardRepository;

    public CardController() {
    }

    private void validate(String cardId) {
        Card card = cardRepository.findOne(cardId);
        if (card == null)
            throw new CardNotFoundException(cardId);
    }

    public List<Card> searchCard(String cardPart) {
        if (cardPart.isEmpty())
            return getCards();
        return cardRepository.findByPart(cardPart);
    }

    public List<Card> getCards() {
        return (List<Card>) cardRepository.findAll();
    }

    public Card getCard(String cardId) {
        validate(cardId);
        Card card = cardRepository.findOne(cardId);
        return card;
    }

    //Calculate all bonus and access bonus
    public CardInfo getCardInfo(String cardId) {
        Card card = getCard(cardId);

        List<Bonus> bonuses = card.getBonus();

        Tarif tarif = card.getTarif();

        int bonusTimeOut = tarif.getBonusTimeout();
        int bonusLockDay = tarif.getBonusLockDays();
        double bonusAll = 0.0,
                bonusAccess = 0.0,
                costAll = 0.0;

        Timestamp now = new Timestamp(new Date().getTime());

        for (Bonus bonus : bonuses) {
            costAll += bonus.getCost();
            int days = getDaysBetween(bonus.getDateOfCost(), now);
            if (days > bonusTimeOut) continue;
            bonusAll += bonus.getBonus();
            if (days < bonusLockDay) continue;
            bonusAccess += bonus.getBonus();
        }

        double persent = 0.0;
        if (costAll <= tarif.getStatusAg()) {
            persent = tarif.getStatusAgPersent();
        } else if (costAll > tarif.getStatusAg() && costAll <= tarif.getStatusAu()) {
            persent = tarif.getStatusAuPersent();
        } else if (costAll > tarif.getStatusAu()) {
            persent = tarif.getStatusPtPersent();
        }

        CardInfo cardInfo = new CardInfo();
        cardInfo.setBonusAccess(bonusAccess).setBonusAll(bonusAll).setPersent(persent).setCostSum(costAll);
        return cardInfo;
    }

    //Расчет дней между датами с учетом знака
    private int getDaysBetween(Timestamp start, Timestamp end) {

        boolean negative = false;
        if (end.before(start)) {
            negative = true;
            Timestamp temp = start;
            start = end;
            end = temp;
        }

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(end);
        calEnd.set(Calendar.HOUR_OF_DAY, 0);
        calEnd.set(Calendar.MINUTE, 0);
        calEnd.set(Calendar.SECOND, 0);
        calEnd.set(Calendar.MILLISECOND, 0);


        if (cal.get(Calendar.YEAR) == calEnd.get(Calendar.YEAR)) {
            if (negative)
                return (calEnd.get(Calendar.DAY_OF_YEAR) - cal.get(Calendar.DAY_OF_YEAR)) * -1;
            return calEnd.get(Calendar.DAY_OF_YEAR) - cal.get(Calendar.DAY_OF_YEAR);
        }

        int days = 0;
        while (calEnd.after(cal)) {
            cal.add(Calendar.DAY_OF_YEAR, 1);
            days++;
        }
        if (negative)
            return days * -1;
        return days;
    }

    //Расчет параметра ордера для 2-х режимов
    public Map<String, Double> calcOrder(String cardId, double cost, String time) {
        Card card = getCard(cardId);
        Tarif tarif = card.getTarif();
        CardInfo cardInfo = getCardInfo(cardId);

        double persent = cardInfo.getPersent();
        double percentOfAmount = tarif.getBonusPartPay();

        //Bonus of time
        double bonusOfTime = 0.0;
        if (tarif.getBonusTimeEnable()) {
            Calendar calendar = Calendar.getInstance();
            Time time0 = tarif.getBonusTimeBegin();
            calendar.setTime(time0);
            int t0 = calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE);//==>1400

            Time time1 = tarif.getBonusTimeEnd();
            calendar.setTime(time1);
            int t1 = calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE);//==>1600

/*
            String ss[] = time.split(":");
            int t = Integer.valueOf(ss[0], 10) * 100 + Integer.valueOf(ss[1], 10);
*/

            DateFormat formatter = new SimpleDateFormat("HH:mm");
            Time vtime = null;
            try {
                vtime = new Time(formatter.parse(time).getTime());
            } catch (ParseException e) {
                throw new TimeConvertException(e);
            }

            calendar.setTime(vtime);
            int t = calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE);

            if (t >= t0 && t <= t1)
                bonusOfTime = tarif.getBonusTimePersent();
        }
        double addBonus = cost / 100.0 * (persent);//сколько бонусов можно получить
        double addTimeBonus = cost / 100.0 * (bonusOfTime);
        double addAllBonus = addBonus + addTimeBonus;

        double applyBonus = (cost / 100.0 * percentOfAmount);//сколько можно заплатить бонусами, если их хватает
        applyBonus = Math.min(applyBonus, cardInfo.getBonusAccess());
        double newCost = cost - applyBonus;


        Map<String, Double> mapCalcOrder = new HashMap<>();
        mapCalcOrder.put("costBonus", addBonus);
        mapCalcOrder.put("timeBonus", addTimeBonus);
        mapCalcOrder.put("allBonus", addAllBonus);
        mapCalcOrder.put("applyBonus", applyBonus);
        mapCalcOrder.put("sumOfCost", newCost);//сумма платежа с учетом бонусов

        return mapCalcOrder;
    }

    public Bonus addBonus(String cardId, double cost, String time) {
        Card card = getCard(cardId);
        Map<String, Double> paramOrder = calcOrder(cardId, cost, time);

        DateFormat formatter = new SimpleDateFormat("HH:mm");
        Time vtime = null;
        try {
            vtime = new Time(formatter.parse(time).getTime());
        } catch (ParseException e) {
            throw new TimeConvertException(e);
        }

        Bonus bonus = new Bonus(cost, vtime, paramOrder.get("allBonus"));
        bonus.setCard(card);
        card.getBonus().add(bonus);

        cardRepository.save(card);
        return bonus;
    }

    public List<Bonus> getAllBonuses(String cardId) {
        Card card = getCard(cardId);
        return card.getBonus();
    }

    public boolean applyBonus(String cardId, double cost, String time) {
        //1 - количество бонусов для списания
        //2 - список транзакций с доступными бонусами в порадке возрвстания даты платежа
        //******************************************************************************

        //??? сумма платежа == 0
        if (Double.compare(cost, 0.0) == 0) return false;

        Card card = getCard(cardId);
        CardInfo cardInfo = getCardInfo(cardId);

        //кол-во доступных бонусов
        double bonusAccess = cardInfo.getBonusAccess();
        //если доступных бонусов ==0
        if (Double.compare(bonusAccess, 0.0) == 0) return false;


        Map<String, Double> paramOrder = calcOrder(cardId, cost, time);


        Tarif tarif = card.getTarif();
        int bonusTimeOut = tarif.getBonusTimeout();
        int bonusLockDay = tarif.getBonusLockDays();


        List<Bonus> bonusList = card.getBonus();
        Collections.reverse(bonusList);

        //сколько нужно списать бонусов
        double bonusForApply = paramOrder.get("applyBonus");

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Timestamp now = new Timestamp(calendar.getTime().getTime());
        //списываем доступные бонусы
        for (Bonus bonus : bonusList) {
            int days = getDaysBetween(bonus.getDateOfCost(), now);
            if (days > bonusTimeOut) continue;
            if (days < bonusLockDay) continue;
            //сколько есть бонусов в транзакции
            double ba = bonus.getBonus();
            int rz = Double.compare(bonusForApply, ba);
            if (rz < 0) {
                bonus.setBonus(ba - bonusForApply);
                bonus.setBonusApply(now);
                bonusForApply = 0.0;
                break;
            }
            if (rz == 0) {
                bonus.setBonus(0.0);
                bonus.setBonusApply(now);
                bonusForApply = 0;
                break;
            }
            if (rz > 0) {
                bonus.setBonus(0.0);
                bonus.setBonusApply(now);
                bonusForApply -= ba;
            }

        }
        //добавим фиктивную запись для учета суммы
        Bonus bonus = new Bonus(paramOrder.get("sumOfCost"), new Time(0), 0.0);
        //bonus.setBonusApply(new Timestamp(0));
        bonus.setCard(card);
        card.getBonus().add(bonus);
        //
        cardRepository.save(card);
        return true;
    }

    public Card save(Card card) {
        return cardRepository.save(card);
    }
}
